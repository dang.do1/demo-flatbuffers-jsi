//
//  RCTDemoModule.h
//  demojsi
//
//  Created by Minh Nguyễn on 19/11/2021.
//

#import <React/RCTBridgeModule.h>
#import "jsi-module.h"

@interface DemoModule : NSObject <RCTBridgeModule>
@property (nonatomic, assign) BOOL setBridgeOnMainQueue;
@end
