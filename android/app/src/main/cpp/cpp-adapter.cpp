#include <jni.h>
#include "jsi-module.h"
#include "pthread.h"
#include <flatbuffers/flatbuffers.h>
#include "log.h"

using namespace std;
using namespace facebook::jsi;
using namespace flatbuffers;

JavaVM *java_vm;
static jobject globalObjectDemoJSIModuleRef;
static jclass globalClassDemoJSIModuleRef;

JNIEnv *GetJniEnv();

extern "C"
JNIEXPORT void JNICALL
Java_com_demojsi_DemoJSIModule_initialize(JNIEnv *env, jobject thiz, jlong jsiPtr) {
    if (!jsiPtr) return;
    auto runtime = reinterpret_cast<facebook::jsi::Runtime *>(jsiPtr);
    installJSIModule(*runtime);

    env->GetJavaVM(&java_vm);
    globalObjectDemoJSIModuleRef = env->NewGlobalRef(thiz);

    auto clazz = env->FindClass("com/demojsi/DemoJSIModule");
    globalClassDemoJSIModuleRef = (jclass) env->NewGlobalRef(clazz);

    auto setUserProfile = Function::createFromHostFunction(
            *runtime,
            PropNameID::forUtf8(*runtime, "setUserProfile"),
            3,
            [](Runtime &runtime, const Value &thisValue, const Value *arguments,
               size_t count) -> Value {
                if (!arguments[0].isObject()) {
                    return Value(-1);
                }
                Object obj = arguments[0].asObject(runtime);
                if (obj.isArrayBuffer(runtime) && arguments[1].isNumber() && arguments[2].isNumber()) {
                    auto bufSize = arguments[1].asNumber();
                    auto offset = arguments[2].asNumber();
                    ArrayBuffer buf = obj.getArrayBuffer(runtime);
                    FlatBufferBuilder fbb;

                    JNIEnv *jniEnv = GetJniEnv();
                    static jmethodID setUserData = jniEnv->GetStaticMethodID(
                            globalClassDemoJSIModuleRef, "setUserData",
                            "(J)V");

                    char *buffer = reinterpret_cast<char *>(buf.data(runtime));
                    jobject byteBufferRef = jniEnv->NewDirectByteBuffer(buffer + (char) offset,
                                                                        bufSize);
                    jniEnv->CallStaticVoidMethod(globalClassDemoJSIModuleRef, setUserData,
                                                 byteBufferRef);

                    jniEnv->DeleteLocalRef(byteBufferRef);
                    return Value(1);
                }
                return Value(-1);
            });
    (*runtime).global().setProperty(*runtime, "setUserProfile", move(setUserProfile));

    auto getUserProfile = Function::createFromHostFunction(
            *runtime,
            PropNameID::forUtf8(*runtime, "getUserProfile"),
            0,
            [](Runtime &runtime, const Value &thisValue, const Value *arguments,
               size_t count) -> Value {

                JNIEnv *jniEnv = GetJniEnv();
                static jmethodID getUserData = jniEnv->GetStaticMethodID(
                        globalClassDemoJSIModuleRef, "getUserData",
                        "()Ljava/nio/ByteBuffer;");
                jobject bufObjecct = jniEnv->CallStaticObjectMethod(globalClassDemoJSIModuleRef,
                                                                    getUserData);

                size_t len = jniEnv->GetDirectBufferCapacity(bufObjecct);
                void *bufRef = jniEnv->GetDirectBufferAddress(bufObjecct);

                auto arrayBuffer = runtime.global().getPropertyAsFunction(runtime, "ArrayBuffer");
                Object o = arrayBuffer.callAsConstructor(runtime,
                                                         {static_cast<double>(len)}).getObject(
                        runtime);
                ArrayBuffer payloadValue = o.getArrayBuffer(runtime);

//                ArrayBuffer *buffer = (ArrayBuffer *) (intptr_t) ch;
//                jniEnv->ReleaseByteArrayElements(bufObjecct, payloadValue, 0);
//                memcpy(&payloadValue, &buffer, len);

                jniEnv->DeleteLocalRef(bufObjecct);
                return payloadValue;
            });
    (*runtime).global().setProperty(*runtime, "getUserProfile", move(getUserProfile));
}

void DeferThreadDetach(JNIEnv *env) {
    static pthread_key_t thread_key;

    // Set up a Thread Specific Data key, and a callback that
    // will be executed when a thread is destroyed.
    // This is only done once, across all threads, and the value
    // associated with the key for any given thread will initially
    // be NULL.
    [] {
        const auto err = pthread_key_create(&thread_key, [](void *ts_env) {
            if (ts_env) {
                java_vm->DetachCurrentThread();
            }
        });
        if (err) {
            // Failed to create TSD key. Throw an exception if you want to.
        }
        return 0;
    }();

    // For the callback to actually be executed when a thread exits
    // we need to associate a non-NULL value with the key on that thread.
    // We can use the JNIEnv* as that value.
    const auto ts_env = pthread_getspecific(thread_key);
    if (!ts_env) {
        if (pthread_setspecific(thread_key, env)) {
            // Failed to set thread-specific value for key. Throw an exception if you
            // want to.
        }
    }
}

JNIEnv *GetJniEnv() {
    JNIEnv *env = nullptr;
    // We still call GetEnv first to detect if the thread already
    // is attached. This is done to avoid setting up a DetachCurrentThread
    // call on a Java thread.

    // g_vm is a global.
    auto get_env_result = java_vm->GetEnv((void **) &env, JNI_VERSION_1_6);
    if (get_env_result == JNI_EDETACHED) {
        if (java_vm->AttachCurrentThread(&env, NULL) == JNI_OK) {
            DeferThreadDetach(env);
        } else {
            // Failed to attach thread. Throw an exception if you want to.
        }
    } else if (get_env_result == JNI_EVERSION) {
        // Unsupported JNI version. Throw an exception if you want to.
    }
    return env;
}


extern "C"
JNIEXPORT void JNICALL
Java_com_demojsi_DemoJSIModule_destruct(JNIEnv *env, jclass clazz) {
    cleanupJSI();
}

extern "C"
JNIEXPORT jobject JNICALL
Java_com_demojsi_DemoJSIModule_getJavaByteBuffer(JNIEnv *env, jclass clazz, jlong address) {
    ArrayBuffer *buffer = (ArrayBuffer *) (intptr_t) address;
    if (buffer == nullptr) {
        __android_log_print(ANDROID_LOG_ERROR, "JSI LOG", "getJavaByteBuffer null with");
        return nullptr;
    }
    return jobject(buffer);
}