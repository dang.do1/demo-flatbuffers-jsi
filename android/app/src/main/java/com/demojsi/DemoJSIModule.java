package com.demojsi;

import android.util.Log;

import androidx.annotation.NonNull;

import com.demojsi.Demo.Profile;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.google.flatbuffers.FlatBufferBuilder;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;

class DemoJSIModule extends ReactContextBaseJavaModule {

    static {
        System.loadLibrary("md5jsi");
    }

    private native void initialize(long jsiPtr);

    private static native void destruct();


    @Override
    public void initialize() {
        super.initialize();
        if (context != null) {
            initialize(context.getJavaScriptContextHolder().get());
        }
    }

    @Override
    public void onCatalystInstanceDestroy() {
        DemoJSIModule.destruct();
    }

    public ReactApplicationContext context;

    public DemoJSIModule(ReactApplicationContext reactContext) {
        this.context = reactContext;
    }

    @NonNull
    @NotNull
    @Override
    public String getName() {
        return "DemoJSIModule";
    }

    public ReactApplicationContext getContext() {
        return context;
    }

    public static Profile profile;

    public static void setUserData(long byteArrayRef) {
        Log.d("userData", "byteArrayRef: " + byteArrayRef);
        ByteBuffer byteBuffer = getJavaByteBuffer(byteArrayRef);
        profile = Profile.getRootAsProfile(byteBuffer);

        Log.d("flat buffer", "agent_id: " + profile.agentId());
        Log.d("flat buffer", "user_name: " + profile.userName());
        Log.d("flat buffer", "user_id: " + profile.userId());
    }

    static public ByteBuffer getUserData() {
        try {
            Log.d("getUserData offset: ", String.valueOf(profile.getByteBuffer()));

        }catch (Exception e) {
            e.printStackTrace();
        }
        return ByteBuffer.allocateDirect(profile.getByteBuffer().capacity());
    }

    public static native ByteBuffer getJavaByteBuffer(long address);


}
