package com.demojsi;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

class DemoModule extends ReactContextBaseJavaModule {

    public ReactApplicationContext context;
    public DemoModule(ReactApplicationContext reactContext) {
        this.context = reactContext;
    }

    @NonNull
    @NotNull
    @Override
    public String getName() {
        return "DemoModule";
    }

    @ReactMethod
    public void getData(String input, Callback cb) {
        StringBuilder builder = new StringBuilder(input);
        builder.append("\nadd them ne!");
        cb.invoke(builder.toString());
    }

    @ReactMethod
    public void hashMd5(String s, Callback cb) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            cb.invoke(hexString.toString());
        } catch (NoSuchAlgorithmException e) {
            cb.invoke("");
        }
    }

}
