#pragma once

#if defined(ANDROID) || defined(__ANDROID__)

#include <android/log.h>

#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, kTAG, __VA_ARGS__))
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, kTAG, __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, kTAG, __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, kTAG, __VA_ARGS__))

#else

#include <stdio.h>
#define LOGD(...) do { printf("[Debug:%s]",kTAG); printf(__VA_ARGS__); printf("\n"); } while(false);
#define LOGI(...) do { printf("[Info:%s]",kTAG); printf(__VA_ARGS__); printf("\n"); } while(false);
#define LOGW(...) do { printf("[Warn:%s]",kTAG); printf(__VA_ARGS__); printf("\n"); } while(false);
#define LOGE(...) do { printf("[Error:%s]",kTAG); printf(__VA_ARGS__); printf("\n"); } while(false);
#endif
