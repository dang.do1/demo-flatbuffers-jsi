import * as flatbuffers from 'flatbuffers';
import {Profile} from './profile';
// import fs from 'fs'

console.info('[FlatBuffer Demo]', 'Start Demo FlatBuffer...');

// Serialize into new flatbuffer.
let fbb = new flatbuffers.Builder();
const nationality = fbb.createString('VietNam');
const userId = fbb.createString('0949852306');
const userName = fbb.createString('Đỗ Khánh Đăng');

Profile.startProfile(fbb);
Profile.addUserId(fbb, userId);
Profile.addAgentId(fbb, 123);
Profile.addNationality(fbb, nationality);
Profile.addFastLogin(fbb, true);
Profile.addUserName(fbb, userName);

let offset = Profile.endProfile(fbb);
fbb.finish(offset);

// const byteBuffer = fbb.dataBuffer();
// let profile = Profile.getRootAsProfile(byteBuffer);
// console.info('[FlatBuffer Demo]', profile)
// console.info('[FlatBuffer Demo]', profile.agentId());
// const dataString = new TextDecoder("utf-8").decode(fbb.asUint8Array());
// fs.writeFileSync('./export/profile_data.bin', fbb.asUint8Array().buffer, 'binary');

console.info(
  'flat offset: ',
  fbb.asUint8Array().buffer.byteLength,
  fbb.asUint8Array().buffer.byteOffset,
);
module.exports = fbb;
